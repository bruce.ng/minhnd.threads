package ch03;

public interface CharacterListener {
  public void newCharacter(CharacterEvent ce);
}
