package ch08.example2;

public interface CharacterListener {
  public void newCharacter(CharacterEvent ce);
}
