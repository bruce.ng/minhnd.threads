package ch04;

public interface ICharacterListener {
  public void newCharacter(CharacterEvent ce);
}
