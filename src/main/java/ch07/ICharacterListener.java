package ch07;

public interface ICharacterListener {
  public void newCharacter(CharacterEvent ce);
}
